var gulp     = require('gulp'), // task runner
    sass     = require('gulp-sass'), // NPM version of SASS
    cssnano  = require('gulp-cssnano'), // Minifies CSS
    uglify   = require('gulp-uglify'), // Minifies Javascript
    gzip     = require('gulp-gzip'), // Compresses assets
    concat   = require('gulp-concat'), // Concatenates JS files into a single file
    babel    = require('gulp-babel'), // For transpiling newer versions of Javascript into legacy JS
    sequence = require('run-sequence'), // For running multiple tasks with one command
    argv     = require('yargs').argv // For adding options to gulp commands
;




// ----------  Setup  ---------- //

// Themes are named after their directories in the application,
// they are used for constructing file paths.

var allowed_themes = ['lion', 'nouvel', 'cwalletsu', 'groupon'];

function show_error( _task ) { // if missing or wrong arguments
  console.log('ERROR: Theme param is not allowed, invalid or missing.');
  console.log('USAGE: gulp ' + _task + ' --theme="<theme name as it appears in the app>"');
  console.log('Available Themes: "' + String(allowed_themes).replace(',', '", "') + '"');
}


// Read the sources.json file in the provided theme.  If it exists and is valid,
// the file paths listed in the sources.json file are parsed first.  If the file
// does not exist or is invalid, it is simply ignored, but will print a warning
// in the console.

function find_sources (_theme) {
  var _library = [];
  try {
    // import a sources.js file from the theme's js directory
    _library = require('./assets/themes/' + argv.theme + '/js/sources.json').SOURCES.core;
  } catch (err) {
    _library = false;
  }
  return _library; // returns either the list of paths from sources.json, or false
}




// ----------  Help  ---------- //

gulp.task('help', function() {
  console.log( help() );
});

gulp.task('default', function()  {
  console.log( help() );
});

function help () {
  var _str = '\n' +
  '  GLOBAL COMMANDS\n' +
  '  ---------------\n' +
  '    gulp build --theme="<theme name>"\n' +
  '      Builds both Javascript and CSS for the given theme.\n' +
  '      * Good for resolving conflicts in minified CSS or JS files.\n' +
  '\n' +
  '    gulp watch --theme="<theme name>"\n' +
  '      Watches BOTH Javascript and CSS files for changes on the given theme, then builds.\n' +
  '      * Warning:  Very memory intensive, can be slow or crash the VM.\n' +
  '\n' +
  '\n' +
  '  CSS COMMANDS\n' +
  '  ------------\n' +
  '    gulp build_css --theme="<theme name>"\n' +
  '      Builds CSS only for the given theme.\n' +
  '      --debug (optional) - Output will not be minified.\n' +
  '        * For debugging on a server.\n' +
  '\n' +
  '    gulp watch_css --theme="<theme name>"\n' +
  '      Watches only CSS for the given theme.\n' +
  '\n' +
  '\n' +
  '  JAVASCRIPT COMMANDS\n' +
  '  -------------------\n' +
  '    gulp build_js --theme="<theme name>"\n' +
  '      Builds only the Javascript for the given theme.\n' +
  '      --debug (optional) - Output will not be minified.\n' +
  '        * For debugging on a server.\n' +
  '\n' +
  '    gulp watch_js --theme="<theme name>"\n' +
  '      Watches only Javascript for the given theme.\n' +
  '\n';
  return _str;
}


// ----------  CSS  ---------- // 

// Runs SASS.
// Using the --debug option will skip the minifcation process
// to aid in debugging.

gulp.task('build_css', function() {
  if (argv.theme && allowed_themes.indexOf(argv.theme) > -1) {
    if (argv.hasOwnProperty('debug')) {
      console.log('DEBUG: CSS will NOT be minified.')
      return gulp.src('assets/sass/themes/' + argv.theme + '/css/**/*.scss')
        .pipe(sass())
        .pipe(gulp.dest('assets/themes/' + argv.theme + '/css'));
    } else {
      return gulp.src('assets/sass/themes/' + argv.theme + '/css/**/*.scss')
        .pipe(sass()) // run sass
        .pipe(cssnano()) // minify it
        .pipe(gulp.dest('assets/themes/' + argv.theme + '/css')); // write the output to the theme's css directory
    }
  } else { show_error('build_css') }
});

gulp.task('watch_css', function() {
  if (argv.theme && allowed_themes.indexOf(argv.theme) > -1) {
    gulp.watch('assets/sass/themes/' + argv.theme + '/css/**/*.scss', ['build_css']);
  } else { show_error('watch_css') }
});




// ----------  JS  ---------- //

// Concatenates and minifies Javascript files.
// * If a sources.json file exists in the theme's js
//   directory, files listed therein will be loaded first.
// * Using the --debug option skips minifaction, which
//   can aid in debugging.

gulp.task('build_js', function() {
  if (argv.theme && allowed_themes.indexOf(argv.theme) > -1) {
    // Tells gulp to search for ALL js files in the theme's
    // js directory.  Prefixing a path with ! will tell gulp
    // to skip that file.
    var _paths = [
      'assets/themes/' + argv.theme + '/**/*.js',
      '!assets/themes/' + argv.theme + '/js/app.min.js'
    ]
    
    var _source_file = find_sources(argv.theme); // use a sources.json file instead of just searching the theme directory
    if (_source_file) _paths = _source_file;
    
    if (argv.hasOwnProperty('debug')) {
      console.log('DEBUG: Javascript will NOT be minified.');
      return gulp.src(_paths)
        .pipe(concat('app.min.js')) // concatenate JS files into a new file called app.min.js
        .pipe(gulp.dest('assets/themes/' + argv.theme + '/js'));  // writes it in the theme's js directory.
    } else {
      return gulp.src(_paths)
        .pipe(concat('app.min.js')) // conactenate JS files into a new file called app.min.js
        .pipe(uglify()) // minifies app.min.js
        .pipe(gulp.dest('assets/themes/' + argv.theme + '/js')); // writes it to the theme's js directory.
    }
  } else { show_error('build_js') }
});


gulp.task('watch_js', function () {
  if (argv.theme && allowed_themes.indexOf(argv.theme) > -1) {
    gulp.watch(['assets/themes/' + argv.theme + '/**/*.js', '!assets/themes/' + argv.theme + '/js/app.min.js'], ['build_js']);
  } else { show_error('watch_js') }
});




// ----------  CSS & JS  ---------- //

gulp.task('build', function () {
  if (argv.theme && allowed_themes.indexOf(argv.theme) > -1) {
    sequence('build_css', 'build_js');
  } else { show_error('build') }
});

gulp.task('watch', function () {
  if (argv.theme && allowed_themes.indexOf(argv.theme) > -1) {
    gulp.watch('assets/sass/themes/' + argv.theme + '/css/**/*.scss', ['build_css']);
    gulp.watch(['assets/themes/' + argv.theme + '/**/*.js', '!assets/themes/' + argv.theme + '/js/app.min.js'], ['build_js']);
  } else { show_error('watch') }
})
