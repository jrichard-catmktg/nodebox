**Description**

A Gulp task runner for compiling CN assets.  Includes a Vagrant file for running from a VM.

**Prerequisites**

1. A working CN environment.
2. Node.js and NPM (If not using the VM)




**NOVICE USERS - Running the compiler from inside a VM**

1. Clone this repository onto your local machine outside of the CN repository, so that it isn't accidently added controlled there. 
2. cd to the project directory and run: vagrant up --provision
3. When the provision has finished, run: vagrant ssh
4. Run: gulp help for a list of commands.

Windows Users:

You'll need to change the synced assets folder in the Vagrant file to match your CN application's assets folder.  (The one containing css, javascript, etc.)




**Updating the VM**

1. cd to the VM's directory.
2. Delete the node_modules folder.
2. Run `$ vagrant halt` to shut down the VM.
3. Run `$ git pull origin master` to pull down the latest version.
4. Run `$ vagrant up --provision` for changes to take affect.




**ADVANCED USERS - Running the compiler locally with Node.js and NPM**

1. Clone this repository onto your local machine outside of the CN repository, so that it isn't accidently version controlled there.
1. Symlink your CN application's assets directory to this directory.  EXAMPLE: `$ ln -s ~/Sites/cn/active/assets`
2. `$ npm install --save-dev`  (You may need to sudo this.)
3. `$ npm run gulp help  // This will output a list of available gulp commands`

**Passing Arguments To NPM Scripts:**

The following would tell gulp to watch all css files in the "nouvel" theme, note the preceeding -- before passing any arguments.

`$ npm run gulp watch_css -- --theme=nouvel`